package br.com.jcneckel.core.serialization

import org.json4s.DefaultFormats

class TDefaultFormats extends DefaultFormats

object TDefaultFormats {
  val defaultFormat = new TDefaultFormats().withLong + new DateSerializer("dd/MM/yyyy HH:mm:ss")

  def apply() = defaultFormat
}