package br.com.jcneckel.core.serialization

import java.util.Date

import br.com.jcneckel.core.util.DateUtils
import org.json4s.CustomSerializer
import org.json4s.JsonAST.{JNull, JString}

class DateSerializer(dateFormater: String) extends CustomSerializer[Date](format =>
  ( {
    case JString(s) => DateUtils.parse(dateFormater, s)
    case JNull => null
  }, {
    case d: Date => JString(DateUtils.format("dd/MM/yyyy HH:mm:ss", d))
  })
)
