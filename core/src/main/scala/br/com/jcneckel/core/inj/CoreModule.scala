package br.com.jcneckel.core.inj

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import br.com.jcneckel.core.authentication.JwtSupport
import br.com.jcneckel.core.database.DatabaseConnector
import br.com.jcneckel.core.serialization.{Json4sSupport, TDefaultFormats}
import br.com.jcneckel.model.repositories.DBRootRespository
import com.typesafe.config.ConfigFactory
import org.json4s.native.Serialization

import scala.concurrent.ExecutionContext

trait CoreModule extends DatabaseConnector with Json4sSupport with JwtSupport {

  implicit val serialization: Serialization.type = Serialization
  implicit val formats = TDefaultFormats()

  implicit val ec = ExecutionContext.Implicits.global
  implicit val system = ActorSystem("data-control")
  implicit val materializer = ActorMaterializer()

  val config = ConfigFactory.load("local.conf")

  object database extends DBRootRespository
}