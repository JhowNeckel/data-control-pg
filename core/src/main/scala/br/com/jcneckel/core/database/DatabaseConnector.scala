package br.com.jcneckel.core.database

import cats.effect._
import doobie._

import scala.concurrent.ExecutionContext

trait DatabaseConnector {

  implicit val cs = IO.contextShift(ExecutionContext.global)

  implicit val xa = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",
    "jdbc:postgresql://ec2-54-83-44-4.compute-1.amazonaws.com:5432/dal2a1csopqp5f",
    "afpldyvaidygrz",
    "e79b277a4488b4945a6d4862e59e6ddb7690e72848ab14ca4506275dd94302e6"
  )
}
