package br.com.jcneckel.core.serialization

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.{FromRequestUnmarshaller, Unmarshaller}
import akka.stream.Materializer

import scala.concurrent.{ExecutionContext, Future}

case class CustomFromRequestUnmarshaller[T](r: String => T) extends FromRequestUnmarshaller[T] {
  override def apply(request: HttpRequest)(implicit ec: ExecutionContext, materializer: Materializer): Future[T] = {
    Unmarshaller.stringUnmarshaller(request.entity).map(r)
  }
}
