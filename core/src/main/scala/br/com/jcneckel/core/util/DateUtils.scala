package br.com.jcneckel.core.util

import java.text.SimpleDateFormat
import java.util.Date

object DateUtils {
  def format(format: String, date: Date) = {
    new SimpleDateFormat(format).format(date)
  }

  def parse(format: String, date: String) = {
    new SimpleDateFormat(format).parse(date)
  }
}
