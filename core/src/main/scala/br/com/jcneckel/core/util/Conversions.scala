package br.com.jcneckel.core.util

object Conversions {

  implicit def optionToVal[T](value: Option[T]) = if (value.isDefined) value.get else throw new IllegalStateException("Valor da variavel option não definido")
}
