package br.com.jcneckel.core.serialization

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypeRange, MediaType}
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, FromRequestUnmarshaller, Unmarshaller}
import akka.util.ByteString
import org.json4s.{Formats, Serialization}

import scala.reflect.Manifest
import scala.collection.immutable.Seq

trait Json4sSupport {

  def unmarshallerContentTypes: Seq[ContentTypeRange] = mediaTypes.map(ContentTypeRange.apply)

  def mediaTypes: Seq[MediaType.WithFixedCharset] = List(`application/json`)

  private val jsonStringUnmarshaller = Unmarshaller.byteStringUnmarshaller.forContentTypes(unmarshallerContentTypes: _*).mapWithCharset {
    case (ByteString.empty, _) => throw Unmarshaller.NoContentException
    case (data, charset) => data.decodeString(charset.nioCharset.name)
  }

  private val jsonStringMarshaller = Marshaller.oneOf(mediaTypes: _*)(Marshaller.stringMarshaller)

  implicit def unmarshaller[A: Manifest](implicit serialization: Serialization, formats: Formats): FromEntityUnmarshaller[A] = {
    jsonStringUnmarshaller.map(s => serialization.read(s))
  }

  implicit def unmarshallerRequest[A: Manifest](implicit serialization: Serialization, formats: Formats): FromRequestUnmarshaller[A] = {
    CustomFromRequestUnmarshaller[A]{json =>
      serialization.read[A](json)
    }
  }

  implicit def marshaller[A <: AnyRef](implicit serialization: Serialization, formats: Formats): ToEntityMarshaller[A] = {
    jsonStringMarshaller.compose(serialization.write[A])
  }
}