package br.com.jcneckel.model.repositories

import br.com.jcneckel.model.entities.User
import cats.effect.IO
import doobie._
import doobie.implicits._
import doobie.util.transactor.Transactor.Aux

trait UserRepository {

  def apply(userId: String)(implicit xa: Aux[IO, Unit]) = {
    Query0[User](s"SELECT ID, NAME, LOGIN, PASSWORD, EMAIL, INCLUDEDAT FROM USERS WHERE ID = '$userId'")
      .option
      .transact(xa)
      .unsafeToFuture()
  }

  def authenticate(username: String, password: String)(implicit xa: Aux[IO, Unit]) = {
    Query0[User](s"SELECT ID, NAME, LOGIN, PASSWORD, EMAIL, INCLUDEDAT FROM USERS WHERE LOGIN = '$username' AND PASSWORD = '$password'")
      .option
      .transact(xa)
      .unsafeToFuture()
  }

  def list(implicit xa: Aux[IO, Unit]) = {
    Query0[User]("SELECT ID, NAME, LOGIN, PASSWORD, EMAIL, INCLUDEDAT FROM USERS").to[List]
      .transact(xa)
      .unsafeToFuture()
  }

  def save(user: User)(implicit xa: Aux[IO, Unit]) = {
    Update[(String, String, String, String, String)](
      s"""
         |INSERT INTO USERS
         |(ID,NAME,LOGIN,PASSWORD,EMAIL,INCLUDEDAT)
         |VALUES
         |(?,?,?,?,?,CURRENT_TIMESTAMP)
       """.stripMargin)
      .run((user.id, user.name, user.login, user.password, user.email))
      .transact(xa)
      .unsafeToFuture()
  }
}
