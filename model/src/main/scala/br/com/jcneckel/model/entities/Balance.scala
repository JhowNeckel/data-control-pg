package br.com.jcneckel.model.entities

case class Balance(userId: String, value: Long)