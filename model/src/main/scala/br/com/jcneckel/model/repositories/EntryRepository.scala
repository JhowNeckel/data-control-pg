package br.com.jcneckel.model.repositories

import br.com.jcneckel.model.entities.Entry
import cats.effect.IO
import doobie._
import doobie.implicits._
import doobie.util.transactor.Transactor.Aux

trait EntryRepository {

  def apply(userId: String, entryId: String)(implicit xa: Aux[IO, Unit]) = {
    Query0[Entry](s"SELECT USERID,ENTRYID,VALUE,DESCRIPTION,DATE,OPERATION FROM ENTRY WHERE USERID = '$userId' AND ENTRYID = '$entryId'")
      .option
      .transact(xa)
      .unsafeToFuture()
  }

  def list(userId: String)(implicit xa: Aux[IO, Unit]) = {
    Query0[Entry](s"SELECT USERID,ENTRYID,VALUE,DESCRIPTION,DATE,OPERATION FROM ENTRY WHERE USERID = '$userId'")
      .to[List]
      .transact(xa)
      .unsafeToFuture()
  }

  def save(entry: Entry)(implicit xa: Aux[IO, Unit]) = {
    Update[(String, Long, String, Int)]("INSERT INTO ENTRY (USERID,VALUE,DESCRIPTION,OPERATION,DATE) VALUES (?,?,?,?,CURRENT_TIMESTAMP)")
      .run((entry.userId, entry.value, entry.description, entry.operation))
      .transact(xa)
      .unsafeToFuture()
  }
}
