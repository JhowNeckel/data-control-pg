package br.com.jcneckel.model.repositories

import br.com.jcneckel.model.entities.Balance
import cats.effect.IO
import doobie._
import doobie.implicits._
import doobie.util.transactor.Transactor.Aux

trait BalanceRepository {

  def apply(userId: String)(implicit xa: Aux[IO, Unit]) = {
    Query0[Balance](s"SELECT USERID,VALUE FROM BALANCE WHERE USERID = '$userId'")
      .option
      .transact(xa)
      .unsafeToFuture()
  }

  def save(balance: Balance)(implicit xa: Aux[IO, Unit]) = {
    Update[Balance]("INSERT INTO BALANCE (USERID,VALUE) VALUES (?,?)")
      .run(balance)
      .transact(xa)
      .unsafeToFuture()
  }

  def update(balance: Balance)(implicit xa: Aux[IO, Unit]) = {
    Update[(Long, String)]("UPDATE BALANCE SET VALUE = ? WHERE USERID = ?")
      .run((balance.value, balance.userId))
      .transact(xa)
      .unsafeToFuture()
  }
}
