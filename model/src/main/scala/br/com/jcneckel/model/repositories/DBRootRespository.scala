package br.com.jcneckel.model.repositories

trait DBRootRespository {
  object user extends UserRepository
  object balance extends BalanceRepository
  object entry extends EntryRepository
}
