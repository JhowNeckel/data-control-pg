package br.com.jcneckel.model.entities

import java.util.Date

case class Entry(userId: String,
                 entryId: Option[String],
                 value: Long,
                 description: String,
                 date: Date,
                 operation: Int)
