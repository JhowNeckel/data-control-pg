import sbt._

object ResolversDefinitions {

  lazy val default = Seq(
    Resolver.mavenLocal,
    Resolver.sbtPluginRepo("releases"),
    "centralrepo" at "http://central.maven.org/maven2/",
    "sonatype-snapshot" at "https://oss.sonatype.org/content/repositories/snapshots/",
    "cakesolutions bintray" at "https://bintray.com/cakesolutions/maven/scala-kafka-client",
    "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
  )
}
