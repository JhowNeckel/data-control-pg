import sbt._

object DependenciesDefinitions {

  lazy val akkaVersion = "2.5.21"
  lazy val httpVersion = "10.1.7"
  lazy val doobieVersion = "0.6.0"
  lazy val configVersion = "1.3.3"
  lazy val json4sVersion = "3.6.5"

  lazy val default = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % httpVersion,
    "org.tpolecat" %% "doobie-core" % doobieVersion,
    "org.tpolecat" %% "doobie-postgres" % doobieVersion,
    "com.typesafe" % "config" % configVersion,
    "org.json4s" %% "json4s-native" % json4sVersion,
    "com.jason-goodwin" %% "authentikat-jwt" % "0.4.5"
  )
}
