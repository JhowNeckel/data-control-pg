lazy val rootSettings = Seq(
  dockerBaseImage := "java:jre-alpine",
  scalaVersion := "2.12.8",
  version := "0.1"  
)

lazy val modules = Seq[ProjectReference](api)

lazy val dataControl = Project("data-control", file(".")).aggregate(modules: _*).settings(rootSettings)

lazy val model = (project in file("model")).settings(defaultSettigns)
lazy val core = (project in file("core")).dependsOn(model)

lazy val api = (project in file("api")).dependsOn(core).enablePlugins(defaultPlugins: _*)

lazy val defaultSettigns = rootSettings ++ Seq(
  libraryDependencies ++= DependenciesDefinitions.default,
  resolvers ++= ResolversDefinitions.default
)

lazy val defaultPlugins = Seq(JavaAppPackaging, DockerPlugin, AshScriptPlugin)

mainClass in Compile := Some("br.com.jcneckel.api.server.WebServer")