package br.com.jcneckel.api.util

case class LoginRequest(username: String, password: String)
