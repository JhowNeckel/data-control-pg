package br.com.jcneckel.api.util

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.StandardRoute
import org.json4s.Formats
import org.json4s.native.Serialization.write

object DefaultResponses {
  val NOT_FOUND = complete(HttpResponse(StatusCodes.NotFound))
  val UNAUTHORIZED = complete(HttpResponse(StatusCodes.Unauthorized))

  def failure(msg: String) = complete(HttpResponse(StatusCodes.InternalServerError, entity = msg))
  def success(msg: String) = complete(HttpResponse(StatusCodes.OK, entity = msg))

  def response[T](value: T)(implicit formats: Formats): StandardRoute = complete {
    HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`, write(value)))
  }
}
