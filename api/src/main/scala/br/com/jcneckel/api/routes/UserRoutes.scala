package br.com.jcneckel.api.routes

import akka.http.scaladsl.server.Directives._
import br.com.jcneckel.api.util.DefaultResponses._
import br.com.jcneckel.core.inj.CoreModule
import br.com.jcneckel.model.entities.{Balance, User}

object UserRoutes extends CoreModule {

  private val pathName = "users"

  private val getUser = get {
    pathPrefix(pathName / Segment) { userId: String =>
      authenticated(_ => onSuccess(database.user(userId))(_.fold(NOT_FOUND)(response)))
    }
  }

  private val listUsers = get {
    path(pathName) {
      authenticated(_ => onSuccess(database.user.list)(response))
    }
  }

  private val saveUser = post {
    path(pathName) {
      authenticated { _ =>
        entity(as[User])(user => onSuccess {
          for {
            _ <- database.user.save(user)
            _ <- database.balance.save(Balance(user.id, 0))
            u <- database.user(user.id)
          } yield u
        }(response))
      }
    }
  }

  val routes = getUser ~ listUsers ~ saveUser
}