package br.com.jcneckel.api.server

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import br.com.jcneckel.api.routes.ServerRoutes
import br.com.jcneckel.core.inj.CoreModule

import scala.util.{Failure, Success}

object WebServer extends CoreModule with App {

  import ServerRoutes._

  val host = config.getString("http.host")
  //  val port = config.getInt("http.port")
  val port = System.getenv("PORT").toInt

  val routes = user ~ balance ~ entry ~ login

  Http().bindAndHandle(routes, host, port) onComplete {
    case Success(binding) => println(s"Server started at ${binding.localAddress}")
    case Failure(e) => println(s"Error on init server. ${e.getMessage}")
  }
}
