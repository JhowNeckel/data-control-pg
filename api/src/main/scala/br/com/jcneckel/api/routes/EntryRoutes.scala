package br.com.jcneckel.api.routes

import akka.http.scaladsl.server.Directives._
import br.com.jcneckel.api.util.DefaultResponses._
import br.com.jcneckel.core.inj.CoreModule
import br.com.jcneckel.model.entities.{Balance, Entry}
import br.com.jcneckel.model.util.EntryOperations.{PAYMENT, RECEIVEMENT}

object EntryRoutes extends CoreModule {

  private val pathName = "entries"

  private val getEntry = get {
    pathPrefix(pathName / Segment / Segment) { (userId: String, entryId: String) =>
      authenticated(_ => onSuccess(database.entry(userId, entryId))(_.fold(NOT_FOUND)(response)))
    }
  }

  private val listEntrees = get {
    pathPrefix(pathName / Segment) { userid: String =>
      authenticated(_ => onSuccess(database.entry.list(userid))(response))
    }
  }

  private val saveEntry = post {
    path(pathName) {
      authenticated { _ =>
        entity(as[Entry])(entry => onSuccess {
          for {
            ob <- database.balance(entry.userId)
            _ <- database.entry.save(entry)
            _ <- database.balance.update(createBalance(entry, ob))
            nb <- database.balance(entry.userId)
          } yield nb
        }(response))
      }
    }
  }

  private def createBalance(entry: Entry, atualBalance: Option[Balance]): Balance = atualBalance match {
    case Some(balance) => Balance(
      userId = entry.userId,
      value = entry.operation match {
        case RECEIVEMENT => balance.value + entry.value
        case PAYMENT => balance.value - entry.value
      }
    )
    case None => throw new IllegalStateException("Balance not found")
  }

  val routes = getEntry ~ listEntrees ~ saveEntry
}
