package br.com.jcneckel.api.routes

import akka.http.scaladsl.server.Directives._
import br.com.jcneckel.api.util.DefaultResponses._
import br.com.jcneckel.core.inj.CoreModule

object BalanceRoutes extends CoreModule {

  private val pathName = "balances"

  private val getBalance = get {
    pathPrefix(pathName / Segment) { userId: String =>
      authenticated(_ => onSuccess(database.balance(userId))(_.fold(NOT_FOUND)(response)))
    }
  }

  val routes = getBalance
}
