package br.com.jcneckel.api.routes

object ServerRoutes {
  val user = UserRoutes.routes
  val entry = EntryRoutes.routes
  val balance = BalanceRoutes.routes
  val login = AuthenticationRoutes.routes
}
