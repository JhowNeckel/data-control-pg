package br.com.jcneckel.api.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import authentikat.jwt.JsonWebToken
import br.com.jcneckel.api.util.LoginRequest
import br.com.jcneckel.core.inj.CoreModule
import br.com.jcneckel.api.util.DefaultResponses._

object AuthenticationRoutes extends CoreModule {

  val pathName = "login"

  private def login: Route = post {
    entity(as[LoginRequest]) { lr =>
      onSuccess(database.user.authenticate(lr.username, lr.password)) {
        case Some(user) =>
          val claims = setClaims(user.login, tokenExpiryPeriodInDays)
          respondWithHeader(RawHeader("X-Access-Token", JsonWebToken(header, claims, secretKey))) {
            response(user)
          }
        case None => UNAUTHORIZED
      }
    }
  }

  val routes = login

}